<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
Route::get('/logout', 'Auth\LoginController@logout');

Route::group(['middleware' => ['auth']], function () {
    Route::get('/','HomeController@index');


    Route::get('/dasboard/get_new_subs','HomeController@get_new_subs');
    Route::get('/dasboard/get_total_subs','HomeController@get_total_subs');
    Route::get('/dasboard/get_deposits','HomeController@get_deposits');
    Route::get('/dasboard/get_profit','HomeController@get_profit');
    Route::get('/dasboard/get_winnings','HomeController@get_winnings');
    Route::get('/dasboard/get_entries','HomeController@get_entries');


    Route::get('/dasboard/get_depositors_summary','HomeController@get_depositors_summary');
    Route::get('/dasboard/get_stakes_summary','HomeController@get_stakes_summary');
    Route::get('/dasboard/get_winners_summary','HomeController@get_winners_summary');



    Route::get('/deposits','TransactionController@deposits')->middleware('perm:6');
    Route::get('/withdrawals','TransactionController@withdrawals')->middleware('perm:7');

    Route::get('/inbox','MessagesController@inbox')->middleware('perm:4');
    Route::get('/outbox','MessagesController@outbox')->middleware('perm:5');


    Route::get('/draws','DrawController@draws')->middleware('perm:2');
    Route::get('/entries','DrawController@entries')->middleware('perm:2');
    Route::get('/winners','DrawController@winners')->middleware('perm:8');


    Route::get('/reports', 'ReportsController@index');//->middleware('perm:9');



    Route::get('/users', 'UserController@index')->middleware('perm:1');
    Route::get('/users/delete/{user_id}', 'UserController@delete_user')->middleware('perm:1');
    Route::post('/enroll', 'UserController@register_user')->middleware('perm:1');
    Route::get('search', 'UserController@getIndex')->middleware('perm:1');
    Route::get('/users/profile/{user_id}', 'UserController@profile')->middleware('perm:1');
    Route::post('/users/profile/update', 'UserController@update')->middleware('perm:1');
    Route::get('/users/groups', 'UserController@groups')->middleware('perm:1');
    Route::post('/users/groups/register', 'UserController@register_group')->middleware('perm:1');
    Route::get('/users/groups/{group_id}', 'UserController@group_profile')->middleware('perm:1');
    Route::get('/users/groups/permissions/delete/{group_id}', 'UserController@delete_group_permission')->middleware('perm:1');
    Route::post('/users/groups/update', 'UserController@update_group')->middleware('perm:1');
    Route::post('/users/groups/permissions/add', 'UserController@add_group_permission')->middleware('perm:1');

});
