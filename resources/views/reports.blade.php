@php
@endphp

@extends('layouts.app')

@section('content')


    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>
                    Rich Box Admin Reports
                </h2>
            </div>

            <!-- Tab -->
            <div class="row">

                <?php

                use Illuminate\Support\Facades\App;


                $engine = App::make("getReporticoEngine");
                $engine->initial_execute_mode = "MENU";
                $engine->access_mode = "ONEPROJECT";
                $engine->initial_project = "Rich Box";
                $engine->initial_project_password = "password"; // If project password required
                $engine->clear_reportico_session = true;
                $engine->execute();
                ?>
            </div>
            <!-- #END# Tab -->


        </div>
    </section>

@endsection