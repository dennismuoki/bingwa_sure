@extends('layouts.app')

@section('content')

    <section class="content">
        <div class="container-fluid">
        <div class="block-header">
            <h2>
                Rich Box Admin
            </h2>
        </div>

            <!-- Tab -->
            <div class="row">

                <div class="row">
                    @if (Session::has('message'))
                        <div class="alert alert-info">{{ Session::get('message') }}</div>
                    @endif
                    @if (Session::has('error'))
                        <div class="alert alert-danger">{{ Session::get('error') }}</div>
                    @endif
                    @if (Session::has('success'))
                        <div class="alert alert-success">{{ Session::get('success') }}</div>
                    @endif

                </div>

                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                {{$usergroup->name}} - Registered Users

                                <br>


                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                            <div class="body">
                                    <table class="table tabe-responsive">
                                        <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Name</th>
                                            <th>E-Mail</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($usergroup->getUsers as $user)
                                            <tr>
                                                <td>
                                                    {{$user->id}}
                                                </td>
                                                <td>
                                                    <a href="{{url('/users/profile/'.($user->id))}}">{{$user->name}}</a>
                                                </td>
                                                <td>{{$user->email}}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>

                                    </table>
                            </div>
                    </div>
                </div>
                <div class=" col-md-6 col-sm-6 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                {{$usergroup->name}} - Group Details

                                <br>

                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                <li role="presentation" class="active"><a href="#permissions" data-toggle="tab">PERMISSIONS</a></li>
                                <li role="presentation"><a href="#addpermissions" data-toggle="tab">ADD PERSMISSIONS</a></li>
                                <li role="presentation"><a href="#editgroup" data-toggle="tab">EDIT</a></li>

                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active" id="permissions">
                                    <b>Group Permissions</b>

                                    <table class="table tabe-responsive">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Description</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($user_permissions as $user_permission)
                                            <tr>
                                                <td>{{$user_permission->get_permission->name}}</td>
                                                <td>{{$user_permission->get_permission->description}}</td>
                                                <td>
                                                    <a href="{{url('/users/groups/permissions/delete/'.$user_permission->id)}}">
                                                        <span class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-trash"></span>
                                                        &nbsp;Delete</span></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>

                                    </table>


                                </div>

                                <div role="tabpanel" class="tab-pane fade" id="addpermissions">
                                    <b>Add Permissions</b>
                                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/users/groups/permissions/add') }}">
                                        {{ csrf_field() }}
                                        {!! Form::hidden('group_id',$usergroup->id) !!}

                                        <div class="modal-body">
                                            <div class="card">
                                                <div class="body">
                                                    <h2 class="card-inside-title">Group Details</h2>



                                                    <div class="row clearfix">
                                                        <div class="col-md-12 {{ $errors->has('permission') ? ' has-error' : '' }}" style="margin-bottom: 0px">
                                                            <div class="input-group input-group-sm">
                                                            <span class="input-group-addon">
                                                                <i class="material-icons">N</i>
                                                            </span>
                                                                <div class="form-line">
                                                                    <select id="permission" name="permission[]" multiple class="populate">
                                                                        @foreach(\App\Permission::all() as $perm)
                                                                            @if(!$usergroup->has_perm([$perm->id],$usergroup->id))
                                                                                <option value="{{$perm->id}}">{{$perm->name}}</option>
                                                                            @endif
                                                                        @endforeach
                                                                    </select>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-success waves-effect">ADD</button>
                                        </div>

                                    </form>
                                </div>

                                <div role="tabpanel" class="tab-pane fade" id="editgroup">
                                    <b>Edit Group</b>
                                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/users/groups/update') }}">
                                        {{ csrf_field() }}
                                        {!! Form::hidden('group_id',$usergroup->id) !!}

                                        <div class="modal-body">
                                            <div class="card">
                                                <div class="body">
                                                    <h2 class="card-inside-title">Group Details</h2>

                                                    <div class="row clearfix">
                                                        <div class="col-md-12 {{ $errors->has('name') ? ' has-error' : '' }}" style="margin-bottom: 0px">
                                                            <div class="input-group input-group-sm">
                                                            <span class="input-group-addon">
                                                                <i class="material-icons">N</i>
                                                            </span>
                                                                <div class="form-line">
                                                                    {!! Form::text('name',$usergroup->name,['class'=>'form-control','required','autofocus', ]) !!}

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="row clearfix">
                                                        <div class="col-md-12 {{ $errors->has('description') ? ' has-error' : '' }}" style="margin-bottom: 0px">
                                                            <div class="input-group input-group-sm">
                                                            <span class="input-group-addon">
                                                                <i class="material-icons">N</i>
                                                            </span>
                                                                <div class="form-line">
                                                                    {!! Form::text('description',$usergroup->description,['class'=>'form-control','required']) !!}

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-success waves-effect">UPDATE</button>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Tab -->


    </div>
    </section>



@endsection

@section('scripts')
    <script>
        function rm(nm,artistID){
            bootbox.confirm("Are you sure you want to delete \"" + nm + "\" ? ", function(result) {
                if(result) {

                    $.ajax({
                        url: 'users/delete/' + artistID,
                        type: 'get',
                        headers: {
                            'X-XSRF-Token': $('meta[name="_token"]').attr('content')
                        },
                        success: function (html) {
                            location.reload();
                        }
                    });
                }
            });
        }


        $(document).ready(function() {
            $.uploadPreview({
                input_field: "#image-upload",
                preview_box: "#image-preview",
                label_field: "#image-label"
            });
        });
    </script>
@endsection
