<html>
<head>
    <meta content="text/html; charset=UTF-8" http-equiv="Content-Type">
</head>
<body leftmargin="0" marginheight="0" marginwidth="0" offset="0" style='width:100%;color:#363636;background-color:#F2F2F2;font-family:"Helvetica Neue",Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;' topmargin="0">
<div style="text-align: center;">
    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
        <tr>
            <td align="center" style="padding-bottom:40px;" valign="top">
                <table border="0" cellpadding="0" cellspacing="0" style="margin-top:40px;margin-left:10px;margin-right:10px;border: 1px solid #e3e3e3;background-color: #FFFFFF;" width="650">
                    <tr>
                        <td width="30">
                            &nbsp;
                        </td>
                        <td align="center" height="90" style="border-bottom: 1px solid #eeeeee;" valign="middle">

                        </td>
                        <td width="30">
                            &nbsp;
                        </td>
                    </tr>
                    <!-- /START BODY -->
                    <tr>
                        <td style="padding-top:30px;padding-bottom:30px;border-bottom: 1px solid #eeeeee;" width="30">
                            &nbsp;
                        </td>
                        <td align="left" style="font-size:14px;padding-top:30px;padding-bottom:30px;border-bottom: 1px solid #eeeeee;" valign="top">
                            <p style="margin:0px;line-height:24px">
                                <b>Welcome to Rich Box Lotto {{$user->name}}!</b>
                                <br>
                                <br>
                                You have been registered as: {{\App\UserGroup::find($user->user_group)->name}}
                                <br>
                                Use the following credentials to <a href="http://104.199.54.177/bingwa_sure/public/" target="_blank"><strong>Log In</strong></a>  to your account.<br>
                                Username: {{$user->email}}<br>
                                Password: {{$user->raw_password}}
                                <br>

                                All the best!
                                <br>
                                <br>
                                Richbox Team<br>
                                Central Command Center<br>
                                <br>
                                <br>
                                Mayfair Business Centre, 3rd Floor, Parklands Road, Nairobi<br>
                                <br>
                                ----------------------------------------------------------------
                                <br>
                                <br>
                                <br>
                            </p>
                        </td>
                        <td style="padding-top:30px;padding-bottom:30px;border-bottom: 1px solid #eeeeee;" width="30">
                            &nbsp;
                        </td>
                    </tr>
                    <!-- /END BODY -->
                </table>
            </td>
        </tr>
    </table>
</div>
</body>
</html>
