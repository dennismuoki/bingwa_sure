@extends('layouts.app')

@section('content')

    <section class="content">
        <div class="container-fluid">
        <div class="block-header">
            <h2>
                Bingwa Sure
            </h2>
        </div>
        <!-- Basic Examples -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Draws
                            <br>

                            @if (Session::has('message'))
                                <div class="alert alert-info">{{ Session::get('message') }}</div>
                            @endif
                            @if (Session::has('error'))
                                <div class="alert alert-danger">{{ Session::get('error') }}</div>
                            @endif
                            @if (Session::has('success'))
                                <div class="alert alert-success">{{ Session::get('success') }}</div>
                            @endif

                            {{--<p class="col-pink">--}}
                                {{--{{$errors->first("name") }}--}}
                                {{--{{$errors->first("stage_name") }}--}}
                                {{--{{$errors->first("artist_art") }}--}}
                            {{--</p>--}}

                        </h2>

                    </div>
                    <div class="body">

                        <div class="table-responsive">
                            <table class="table table-striped table-hover js-basic-example">
                                <thead>
                                <tr>
                                    <th>Phone No.</th>
                                    <th>Stake</th>
                                    <th>Choice</th>
                                    <th>Option A</th>
                                    <th>Option B</th>
                                    <th>Option C</th>
                                    <th>Option D</th>
                                    <th>Option E</th>
                                    <th>Option F</th>
                                    <th>Option G</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>Phone No.</th>
                                    <th>Stake</th>
                                    <th>Choice</th>
                                    <th>Option A</th>
                                    <th>Option B</th>
                                    <th>Option C</th>
                                    <th>Option D</th>
                                    <th>Option E</th>
                                    <th>Option F</th>
                                    <th>Option G</th>
                                </tr>
                                </tfoot>
                                <tbody>
                                    @foreach($draws as $draw)
                                        <tr>
                                            <td>
                                                {{
                                                \App\Entry::find($draw->entry_id) ?
                                                        \App\Profile::find(\App\Entry::find($draw->entry_id)->profile_id ) ?
                                                            \App\Profile::find(\App\Entry::find($draw->entry_id)->profile_id )->msisdn
                                                        : ""
                                                 : ""
                                                 }}
                                            </td>
                                            <td>{{\App\Entry::find($draw->entry_id) ? \App\Entry::find($draw->entry_id)->stake : ""}}</td>
                                            <td>{{\App\Entry::find($draw->entry_id) ? \App\Entry::find($draw->entry_id)->choice : ""}}</td>
                                            <td>{{$draw->option_a}}</td>
                                            <td>{{$draw->option_b}}</td>
                                            <td>{{$draw->option_c}}</td>
                                            <td>{{$draw->option_d}}</td>
                                            <td>{{$draw->option_e}}</td>
                                            <td>{{$draw->option_f}}</td>
                                            <td>{{$draw->option_g}}</td>


                                        </tr>

                                    @endforeach
                                </tbody>
                            </table>
                            <div style="text-align: center;">{{$draws->links()}}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Basic Examples -->
    </div>
    </section>



@endsection

@section('scripts')
    <script>
        function rm(nm,artistID){
            bootbox.confirm("Are you sure you want to delete \"" + nm + "\" ? ", function(result) {
                if(result) {

                    $.ajax({
                        url: '/artists/delete/' + artistID,
                        type: 'get',
                        headers: {
                            'X-XSRF-Token': $('meta[name="_token"]').attr('content')
                        },
                        dataType: 'html',
                        success: function(html) {
                            bootbox.alert("Deleted successfully");
                            window.location = 'artists';
                        }
                    });
                }
            });
        }
    </script>
@endsection
