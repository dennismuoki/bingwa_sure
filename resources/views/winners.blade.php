@extends('layouts.app')

@section('content')

    <section class="content" style="margin-top: 0px">
        <div class="container-fluid">

        <!-- Basic Examples -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="body">

                        <div class="table-responsive">
                            {!! $grid !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Basic Examples -->
    </div>
    </section>



@endsection

@section('scripts')
    <script>
        function rm(nm,artistID){
            bootbox.confirm("Are you sure you want to delete \"" + nm + "\" ? ", function(result) {
                if(result) {

                    $.ajax({
                        url: '/artists/delete/' + artistID,
                        type: 'get',
                        headers: {
                            'X-XSRF-Token': $('meta[name="_token"]').attr('content')
                        },
                        dataType: 'html',
                        success: function(html) {
                            bootbox.alert("Deleted successfully");
                            window.location = 'artists';
                        }
                    });
                }
            });
        }
    </script>
@endsection
