@extends('layouts.app')
@php
    use Illuminate\Support\Facades\App;

    $chartdeposits = \App\Deposit::select(DB::raw('HOUR(created_at) as hour,  SUM(amount) as amount'))->whereDate('created_at',\Carbon\Carbon::now()->toDateString())->groupBy('hour')->get();
    $chartdepositsyesterday = \App\Deposit::select(DB::raw('HOUR(created_at) as hour,  SUM(amount) as amount'))->whereDate('created_at',\Carbon\Carbon::now()->subDay(1)->toDateString())->groupBy('hour')->get();

@endphp
@section('content')
  <section class="content">
    <div class="container-fluid">
      <div class="block-header">
        <h4 style="color: red">Stats as of {{\Carbon\Carbon::parse(\Carbon\Carbon::now())->formatLocalized('%A %d %B %Y')}}</h4>
      </div>

      <!-- Widgets -->
        <div class="row clearfix">

          @if (Session::has('message'))
            <div class="alert alert-info">{{ Session::get('message') }}</div>
          @endif
          @if (Session::has('error'))
            <div class="alert alert-danger">{{ Session::get('error') }}</div>
          @endif
          @if (Session::has('success'))
            <div class="alert alert-success">{{ Session::get('success') }}</div>
          @endif



              <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-light-green hover-expand-effect" id="newSubsID">
                  <div class="icon">
                    <i class="material-icons">group_add</i>
                  </div>
                  <div class="content">
                    <div class="text">NEW SUBSCRIBERS</div>
                    <div class="number" id="newSubs">
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" >
                <div class="info-box bg-green hover-expand-effect" id="allSubsID">
                  <div class="icon">
                    <i class="material-icons">supervisor_account</i>
                  </div>
                  <div class="content">
                    <div class="text">TOTAL SUBSCRIBERS</div>
                    <div class="number" id="allSubs" >
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" >
                <div class="info-box bg-pink hover-expand-effect" id="depositsID">
                  <div class="icon">
                    <i class="material-icons">assignment_returned</i>
                  </div>
                  <div class="content">
                    <div class="text">DEPOSITS(KSH)</div>
                    <div class="number" id="deposits">
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" >
                <div class="info-box bg-orange hover-expand-effect" id="netProfitID">
                  <div class="icon">
                    <i class="material-icons">assessment</i>
                  </div>
                  <div class="content">
                    <div class="text">TODAY'S NET PROFIT</div>
                    <div class="number" id="netProfit">
                    </div>
                  </div>
                </div>
              </div>
        </div>
        <!-- #END# Widgets -->

        <!-- Stats -->
        <div class="row clearfix">
          <!-- Visitors -->
          <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
              <div class="card">
                <div class="body bg-pink">
                  <div style="margin-bottom: 20px" class="m-b--35 font-bold">DEPOSITS HISTORY</div>

                  <div class="sparkline" data-type="line" data-spot-Radius="4" data-highlight-Spot-Color="rgb(233, 30, 99)" data-highlight-Line-Color="#fff"
                       data-min-Spot-Color="rgb(255,255,255)" data-max-Spot-Color="rgb(255,255,255)" data-spot-Color="rgb(255,255,255)"
                       data-offset="90" data-width="100%" data-height="100px" data-line-Width="2" data-line-Color="rgba(255,255,255,0.7)"
                       data-fill-Color="rgba(0, 188, 212, 0)">

                    @php
                      $numItems = count($deposits);
                      $i = 0;
                    @endphp

                    @foreach($deposits as $deposit)
                        {{$deposit->amount}}
                      @php
                      echo (++$i === $numItems ? "" : ",");

                      @endphp

                    @endforeach
                  </div>
                  <ul class="dashboard-stat-list">
                    <li>
                      LAST 7 DAYS' DEPOSITS
                    </li>
                  </ul>
                </div>
              </div>
          </div>
          <!-- #END# Visitors -->
          <!-- Latest Social Trends -->
          <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
            <div class="card" id="winningCard">
              <div class="body bg-cyan">
                <div class="m-b--35 font-bold">
                    WINNINGS HISTORY
                </div>

                <ul class="dashboard-stat-list" id="winningsList">
                    <li>Daily summary of deposits</li>
                    <li></li>

                </ul>
              </div>
            </div>
          </div>
          <!-- #END# Latest Social Trends -->
          <!-- Answered Tickets -->
          <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
            <div class="card" id="entriesCard">
              <div class="body bg-teal">
                <div class="font-bold m-b--35">ENTRIES HISTORY</div>
                <ul class="dashboard-stat-list" id="entriesList">
                    <li>Daily summary of entries</li>
                    <li></li>
                </ul>
              </div>
            </div>
          </div>
          <!-- #END# Answered Tickets -->
        </div>
        <!-- #END# Stats -->

      <!-- more stats - With Loading -->
      <div class="row clearfix">
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
          <div class="card">
            <div class="header">
              <h2>
                Deposits <small>Summary of depositors</small>
              </h2>
              <ul class="header-dropdown m-r--5">
                <li>
                  <h2 id="depositsPercentageButton">

                    {{--<small>Conversion</small>--}}
                  </h2>
                </li>
                  <li>
                      <a onclick="refreshDepositors()">
                          <i class="material-icons">loop</i>
                      </a>
                  </li>
              </ul>
            </div>
            <div class="body" style="padding: 0px">

              <div style="padding-left: 20px" class="table-responsive" id="depositorsOverview">
                <table class="table table-striped">
                  <tbody>
                    <tr>
                      <td>New</td>
                      <td>0</td>
                      <td>Ksh. 0</td>
                    </tr>
                    <tr>
                      <td>Existing</td>
                      <td>0</td>
                      <td>Ksh. 0</td>
                    </tr>
                    <tr>
                      <td><b>Total</b></td>
                      <td><b>0</b></td>
                      <td><b>Ksh. 0</b></td>
                    </tr>

                    <tr>
                      <td><b>Target</b></td>
                      <td><b>Ksh. {{Config::get('app.target')}}</b></td>
                      <td><b class="bg-green" style="padding: 5px">0</b></td>
                    </tr>
                  </tbody>
                </table>
              </div>

            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
          <div class="card">
            <div class="header">
              <h2>
                Players Stake <small>Summary of stakes placed</small>
              </h2>

              <ul class="header-dropdown m-r--5">
                <li>
                  <h2 id="stakesPercentageButton">
                  </h2>
                </li>
                  <li>
                      <a onclick="refreshStake()">
                          <i class="material-icons">loop</i>
                      </a>
                  </li>
              </ul>
            </div>
            <div class="body" style="padding: 0px">
              <div style="padding-left: 20px" class="table-responsive" id="stakesOverview">
                <table class="table table-striped">
                  <tbody>
                  <tr>
                    <td>New</td>
                    <td>0</td>
                    <td>Ksh. 0</td>
                  </tr>
                  <tr>
                    <td>Existing</td>
                    <td>0</td>
                    <td>Ksh. 0</td>
                  </tr>
                  <tr>
                    <td><b>Total</b></td>
                    <td><b>0</b></td>
                    <td><b>Ksh. 0</b></td>
                  </tr>
                  <tr>
                      <td><b>Target</b></td>
                      <td><b>Ksh. {{Config::get('app.target')}}</b></td>
                      <td><b class="bg-green" style="padding: 5px">0</b></td>
                  </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
          <div class="card">
            <div class="header">
              <h2>
                Winners <small>Summary of winners</small>
              </h2>

              <ul class="header-dropdown m-r--5">
                <li>
                  <h2 id="winnersPercentageButton">
                  </h2>
                </li>
                  <li>
                      <a onclick="refreshWinners()">
                          <i class="material-icons">loop</i>
                      </a>
                  </li>
              </ul>
            </div>
            <div class="body" style="padding: 0px">
              <div style="padding-left: 20px" class="table-responsive">


                  <table class="table table-striped" id="winnersOverview">
                      <tbody>
                          <tr>
                              <td>New</td>
                              <td>0</td>
                              <td>Ksh. 0</td>
                          </tr>
                          <tr>
                              <td>Existing</td>
                              <td>0</td>
                              <td>Ksh. 0</td>
                          </tr>
                          <tr>
                              <td><b>Total</b></td>
                              <td><b>0</b></td>
                              <td><b>Ksh. 0</b></td>
                          </tr>

                          <tr>
                              <td><b>Winning Rate</b></td>
                              <td><b> </b></td>
                              <td>
                                 0
                              </td>
                          </tr>
                      </tbody>

                  </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- #END# more stats - With Loading -->


        <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header" style="padding-bottom: 5px">
                            <h2>
                                HOURLY DEPOSITS CHART
                            </h2>
                        </div>
                        <div class="body" style="padding-top: 5px">

                            <div id="all_chart" class="flot-chart" ></div>
                        </div>
                    </div>
                </div>
            </div>



      <!-- mpesa same time -->
      <div class="row clearfix">
        <!-- Task Info -->
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="card">
            <div class="header">
              <h2>Daily Deposits</h2>
              <small>A summary of the daily deposits and same time deposits</small>
              <ul class="header-dropdown m-r--5">
                <li class="dropdown">
                  <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                    <i class="material-icons">more_vert</i>
                  </a>
                  <ul class="dropdown-menu pull-right">
                    <li><a href="javascript:void(0);">Action</a></li>
                    <li><a href="javascript:void(0);">Another action</a></li>
                    <li><a href="javascript:void(0);">Something else here</a></li>
                  </ul>
                </li>
              </ul>
            </div>
            <div class="body">
              <div class="table-responsive ">

                  <?php

                  $engine = App::make("getReporticoEngine");
                  $engine->initial_execute_mode = "EXECUTE";
                  $engine->initial_report = "Summary Daily Deposits.xml";
                  $engine->access_mode = "ONEREPORT";
                  $engine->initial_project = "Rich Box";
                  $engine->initial_project_password = "r1chb0x"; // If project password required
                  $engine->clear_reportico_session = true;
                  $engine->execute();
                  ?>

              </div>

            </div>
          </div>
        </div>
        <!-- #END# Task Info -->
      </div>
      <!-- #END# mpesa same time-->

    </div>
  </section>

@endsection


@section('css')
  <!-- Morris Chart Css-->
  <link href="{{url('plugins/morrisjs/morris.css')}}" rel="stylesheet" />
  <link href="{{url('plugins/waitme/waitMe.css')}}" rel="stylesheet" />

@endsection

@section('scripts')
  <script src="{{url('plugins/jquery-sparkline/jquery.sparkline.js')}}"></script>
  <!-- Morris Plugin Js -->
  <script src="{{url('plugins/raphael/raphael.min.js')}}"></script>
  <script src="{{url('plugins/morrisjs/morris.js')}}"></script>

  <!-- ChartJs -->
  <script src="{{url('plugins/chartjs/Chart.bundle.js')}}"></script>
  <script src="{{url('plugins/waitme/waitMe.js')}}"></script>
  <script src="{{url('js/pages/cards/colored.js')}}"></script>





  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

  <script type="text/javascript">


      var effect = 'timer'; $(this).data('loadingEffect');
      var color = $.AdminBSB.options.colors['lightBlue'];

      var $winningsLoading = $("#winningCard").waitMe({
          effect: effect,
          text: 'Loading...',
          bg: 'rgba(255,255,255,0.90)',
          color: color
      });

      var $entriesLoading = $("#entriesCard").waitMe({
          effect: effect,
          text: 'Loading...',
          bg: 'rgba(255,255,255,0.90)',
          color: color
      });

      var $allSubsLoading = $("#allSubsID").waitMe({
          effect: effect,
          //text: 'Loading...',
          bg: 'rgba(255,255,255,0.90)',
          color: color
      });

      var $newSubsLoading = $("#newSubsID").waitMe({
          effect: effect,
          // text: 'Loading...',
          bg: 'rgba(255,255,255,0.90)',
          color: color
      });

      var $depositsLoading = $("#depositsID").waitMe({
          effect: effect,
          // text: 'Loading...',
          bg: 'rgba(255,255,255,0.90)',
          color: color
      });

      var $netProfitLoading = $("#netProfitID").waitMe({
          effect: effect,
          //text: 'Loading...',
          bg: 'rgba(255,255,255,0.90)',
          color: color
      });

      var $depositorsOverviewLoading = $("#depositorsOverview").waitMe({
          effect: effect,
          text: 'Loading...',
          bg: 'rgba(255,255,255,0.90)',
          color: color
      });

      var $stakesOverviewLoading = $("#stakesOverview").waitMe({
          effect: effect,
          text: 'Loading...',
          bg: 'rgba(255,255,255,0.90)',
          color: color
      });

      var $winnersOverviewLoading = $("#winnersOverview").waitMe({
          effect: effect,
          text: 'Loading...',
          bg: 'rgba(255,255,255,0.90)',
          color: color
      });


      $(document).ready(function(){



          getNewSubs();
          getAllSubs();
          getDeposits();
          getProfit();
          getWinnings();
          getEntries();
          getDepositorsOverview();
          getStakesOverview();
          getWinnersOverview();

      });


      google.charts.load('current', {'packages':['line']});
      google.charts.setOnLoadCallback(drawAllChart);

      function drawAllChart() {
          var data = google.visualization.arrayToDataTable([
              ['Hour',  'Today','Yesterday'],
              ['0',   {{$chartdeposits[0]->amount}}, {{$chartdepositsyesterday[0]->amount ? $chartdepositsyesterday[0]->amount : 0}}],
              ['1',   {{$chartdeposits[1]->amount}}, {{$chartdepositsyesterday[1]->amount ? $chartdepositsyesterday[1]->amount : 0}}],
              ['2',   {{$chartdeposits[2]->amount}}, {{$chartdepositsyesterday[2]->amount ? $chartdepositsyesterday[2]->amount : 0}}],
              ['3',   {{$chartdeposits[3]->amount}}, {{$chartdepositsyesterday[3]->amount ? $chartdepositsyesterday[3]->amount : 0}}],
              ['4',   {{$chartdeposits[4]->amount}}, {{$chartdepositsyesterday[4]->amount ? $chartdepositsyesterday[4]->amount : 0}}],
              ['5',   {{$chartdeposits[5]->amount}}, {{$chartdepositsyesterday[5]->amount ? $chartdepositsyesterday[5]->amount : 0}}],
              ['6',   {{$chartdeposits[6]->amount}}, {{$chartdepositsyesterday[6]->amount ? $chartdepositsyesterday[6]->amount : 0}}],
              ['7',   {{$chartdeposits[7]->amount}}, {{$chartdepositsyesterday[7]->amount ? $chartdepositsyesterday[7]->amount : 0}}],
              ['8',   {{$chartdeposits[8]->amount}}, {{$chartdepositsyesterday[8]->amount ? $chartdepositsyesterday[8]->amount : 0}}],
              ['9',   {{$chartdeposits[9]->amount}}, {{$chartdepositsyesterday[9]->amount ? $chartdepositsyesterday[9]->amount : 0}}],
              ['10',   {{$chartdeposits[10]->amount}}, {{$chartdepositsyesterday[10]->amount ? $chartdepositsyesterday[10]->amount : 0}}],
              ['11',   {{$chartdeposits[11]->amount}}, {{$chartdepositsyesterday[11]->amount ? $chartdepositsyesterday[11]->amount : 0}}],
              ['12',   {{$chartdeposits[12]->amount}}, {{$chartdepositsyesterday[12]->amount ? $chartdepositsyesterday[12]->amount : 0}}],
              ['13',   {{$chartdeposits[13]->amount}}, {{$chartdepositsyesterday[13]->amount ? $chartdepositsyesterday[13]->amount : 0}}],
              ['14',   {{$chartdeposits[14]->amount}}, {{$chartdepositsyesterday[14]->amount ? $chartdepositsyesterday[14]->amount : 0}}],
              ['15',   {{$chartdeposits[15]->amount}}, {{$chartdepositsyesterday[15]->amount ? $chartdepositsyesterday[15]->amount : 0}}],
              ['16',   {{$chartdeposits[16]->amount}}, {{$chartdepositsyesterday[16]->amount ? $chartdepositsyesterday[16]->amount : 0}}],
              ['17',   {{$chartdeposits[17]->amount}}, {{$chartdepositsyesterday[17]->amount ? $chartdepositsyesterday[17]->amount : 0}}],
              ['18',   {{$chartdeposits[18]->amount}}, {{$chartdepositsyesterday[18]->amount ? $chartdepositsyesterday[18]->amount : 0}}],
              ['19',   {{$chartdeposits[19]->amount}}, {{$chartdepositsyesterday[19]->amount ? $chartdepositsyesterday[19]->amount : 0}}],
              ['20',   {{$chartdeposits[20]->amount}}, {{$chartdepositsyesterday[20]->amount ? $chartdepositsyesterday[20]->amount : 0}}],
              ['21',   {{$chartdeposits[21]->amount}}, {{$chartdepositsyesterday[21]->amount ? $chartdepositsyesterday[21]->amount : 0}}],
              ['22',   {{$chartdeposits[22]->amount}}, {{$chartdepositsyesterday[22]->amount ? $chartdepositsyesterday[22]->amount : 0}}],
              ['23',   {{$chartdeposits[23]->amount}}, {{$chartdepositsyesterday[23]->amount ? $chartdepositsyesterday[23]->amount : 0}}]
          ]);


          var options = {
              curveType: 'function',
              legend: { position: 'bottom' }
          };

          var chart = new google.charts.Line(document.getElementById('all_chart'));
          chart.draw(data, options);
      }

      function getNewSubs() {
          var newSubs = $("#newSubs");
          $.ajax({
              type: "GET",
              url: "{!! url('dasboard/get_new_subs/') !!}",
              data: {"_token": "{{ csrf_token() }}"},
              //timeout: 5000,
              success: function (resp) {
                  console.log(resp);
                  resp = JSON.parse(resp);
                  if (resp.error) {
                      newSubs.html(resp.message);
                      $newSubsLoading.waitMe('hide');
                  } else {
                      newSubs.html(resp.message);
                      $newSubsLoading.waitMe('hide');
                  }
              },
              error: function (jqXHR, textStatus, errorThrown) {
                  if (textStatus === "timeout") {
                      newSubs.html("Timeout Error. Please try again");
                  } else {
                      newSubs.html(errorThrown);
                  }
              }
          });

      }

      function getAllSubs() {

          var allSubs = $("#allSubs");
          $.ajax({
              type: "GET",
              url: "{!! url('dasboard/get_total_subs/') !!}",
              data: {"_token": "{{ csrf_token() }}"},
              //timeout: 5000,
              success: function (resp) {
                  console.log(resp);
                  resp = JSON.parse(resp);
                  if (resp.error) {
                      allSubs.html(resp.message);
                      $allSubsLoading.waitMe('hide');
                  } else {
                      allSubs.html(resp.message);
                      $allSubsLoading.waitMe('hide');
                  }
              },
              error: function (jqXHR, textStatus, errorThrown) {
                  if (textStatus === "timeout") {
                      allSubs.html("Timeout Error. Please try again");
                  } else {
                      allSubs.html(errorThrown);
                  }
              }
          });

      }

      function getDeposits() {

          var deposits = $("#deposits");
          $.ajax({
              type: "GET",
              url: "{!! url('dasboard/get_deposits/') !!}",
              data: {"_token": "{{ csrf_token() }}"},
              //timeout: 5000,
              success: function (resp) {
                  console.log(resp);
                  resp = JSON.parse(resp);
                  if (resp.error) {
                      deposits.html(resp.message);
                      $depositsLoading.waitMe('hide');
                  } else {
                      deposits.html(resp.message);
                      $depositsLoading.waitMe('hide');
                  }
              },
              error: function (jqXHR, textStatus, errorThrown) {
                  if (textStatus === "timeout") {
                      deposits.html("Timeout Error. Please try again");
                  } else {
                      deposits.html(errorThrown);
                  }
              }
          });

      }

      function getProfit() {

          var netProfit = $("#netProfit");
          $.ajax({
              type: "GET",
              url: "{!! url('dasboard/get_profit/') !!}",
              data: {"_token": "{{ csrf_token() }}"},
              //timeout: 5000,
              success: function (resp) {
                  console.log(resp);
                  resp = JSON.parse(resp);
                  if (resp.error) {
                      netProfit.html(resp.message);
                      $netProfitLoading.waitMe('hide');
                  } else {
                      netProfit.html(resp.message);
                      $netProfitLoading.waitMe('hide');
                  }
              },
              error: function (jqXHR, textStatus, errorThrown) {
                  if (textStatus === "timeout") {
                      netProfit.html("Timeout Error. Please try again");
                  } else {
                      netProfit.html(errorThrown);
                  }
              }
          });

      }

      function getWinnings() {

          var winningsList = $("#winningsList");
          $.ajax({
              type: "GET",
              url: "{!! url('dasboard/get_winnings/') !!}",
              data: {"_token": "{{ csrf_token() }}"},
              //timeout: 5000,
              success: function (resp) {
                  console.log(resp);
                  resp = JSON.parse(resp);
                  if (resp.error) {
                      winningsList.html(resp.message);
                      $winningsLoading.waitMe('hide');
                  } else {
                      winningsList.html(resp.message);
                      $winningsLoading.waitMe('hide');
                  }
              },
              error: function (jqXHR, textStatus, errorThrown) {
                  if (textStatus === "timeout") {
                      winningsList.html("Timeout Error. Please try again");
                  } else {
                      winningsList.html(errorThrown);
                  }
              }
          });

      }

      function getEntries() {

          var entriesList = $("#entriesList");
          $.ajax({
              type: "GET",
              url: "{!! url('dasboard/get_entries/') !!}",
              data: {"_token": "{{ csrf_token() }}"},
              //timeout: 5000,
              success: function (resp) {
                  console.log(resp);
                  resp = JSON.parse(resp);
                  if (resp.error) {
                      entriesList.html(resp.message);
                      $entriesLoading.waitMe('hide');
                  } else {
                      entriesList.html(resp.message);
                      $entriesLoading.waitMe('hide');
                  }
              },
              error: function (jqXHR, textStatus, errorThrown) {
                  if (textStatus === "timeout") {
                      entriesList.html("Timeout Error. Please try again");
                  } else {
                      entriesList.html(errorThrown);
                  }
              }
          });

      }

      function getDepositorsOverview() {

          var depositorsOverview = $("#depositorsOverview");
          var depositsPercentageButton = $("#depositsPercentageButton");
          $.ajax({
              type: "GET",
              url: "{!! url('dasboard/get_depositors_summary/') !!}",
              data: {"_token": "{{ csrf_token() }}"},
              //timeout: 5000,
              success: function (resp) {
                  console.log(resp);
                  resp = JSON.parse(resp);
                  if (resp.error) {
                      depositorsOverview.html(resp.table);
                      depositsPercentageButton.html(resp.percentageButton);
                      $depositorsOverviewLoading.waitMe('hide');
                  } else {
                      depositorsOverview.html(resp.table);
                      depositsPercentageButton.html(resp.percentageButton);
                      $depositorsOverviewLoading.waitMe('hide');
                  }
              },
              error: function (jqXHR, textStatus, errorThrown) {
                  if (textStatus === "timeout") {
                      depositorsOverview.html("Timeout Error. Please try again");
                  } else {
                      depositorsOverview.html(errorThrown);
                  }
              }
          });

      }

      function getStakesOverview() {

          var stakesOverview = $("#stakesOverview");
          var stakesPercentageButton = $("#stakesPercentageButton");

          $.ajax({
              type: "GET",
              url: "{!! url('dasboard/get_stakes_summary/') !!}",
              data: {"_token": "{{ csrf_token() }}"},
              //timeout: 5000,
              success: function (resp) {
                  console.log(resp);
                  resp = JSON.parse(resp);
                  if (resp.error) {
                      stakesOverview.html(resp.table);
                      stakesPercentageButton.html(resp.percentageButton);
                      $stakesOverviewLoading.waitMe('hide');
                  } else {
                      stakesOverview.html(resp.table);
                      stakesPercentageButton.html(resp.percentageButton);
                      $stakesOverviewLoading.waitMe('hide');
                  }
              },
              error: function (jqXHR, textStatus, errorThrown) {
                  if (textStatus === "timeout") {
                      stakesOverview.html("Timeout Error. Please try again");
                  } else {
                      stakesOverview.html(errorThrown);
                  }
              }
          });

      }

      function getWinnersOverview() {

          var winnersOverview = $("#winnersOverview");
          var winnersPercentageButton = $("#winnersPercentageButton");

          $.ajax({
              type: "GET",
              url: "{!! url('dasboard/get_winners_summary/') !!}",
              data: {"_token": "{{ csrf_token() }}"},
              //timeout: 5000,
              success: function (resp) {
                  console.log(resp);
                  resp = JSON.parse(resp);
                  if (resp.error) {
                      winnersOverview.html(resp.table);
                      winnersPercentageButton.html(resp.percentageButton);
                      $winnersOverviewLoading.waitMe('hide');
                  } else {
                      winnersOverview.html(resp.table);
                      winnersPercentageButton.html(resp.percentageButton);
                      $winnersOverviewLoading.waitMe('hide');
                  }
              },
              error: function (jqXHR, textStatus, errorThrown) {
                  if (textStatus === "timeout") {
                      winnersOverview.html("Timeout Error. Please try again");
                  } else {
                      winnersOverview.html(errorThrown);
                  }
              }
          });

      }

      function refreshDepositors() {
          $depositorsOverviewLoading.waitMe({
              effect: effect,
              text: 'Refreshing...',
              bg: 'rgba(255,255,255,0.90)',
              color: color
          });
          getDepositorsOverview();
      }

      function refreshStake() {
          $stakesOverviewLoading.waitMe({
              effect: effect,
              text: 'Refreshing...',
              bg: 'rgba(255,255,255,0.90)',
              color: color
          });
          getStakesOverview();
      }
      
      function refreshWinners() {
          $winnersOverviewLoading.waitMe({
              effect: effect,
              text: 'Refreshing...',
              bg: 'rgba(255,255,255,0.90)',
              color: color
          });
          getWinnersOverview();
      }
  </script>




@endsection
