@extends('layouts.app')

@section('content')
  <section class="content">
    <div class="container-fluid">
      <div class="block-header">
        <h4 style="color: red">Stats as of {{\Carbon\Carbon::parse(\Carbon\Carbon::now())->formatLocalized('%A %d %B %Y')}}</h4>
      </div>

      <!-- Widgets -->
        <div class="row clearfix">

          @if (Session::has('message'))
            <div class="alert alert-info">{{ Session::get('message') }}</div>
          @endif
          @if (Session::has('error'))
            <div class="alert alert-danger">{{ Session::get('error') }}</div>
          @endif
          @if (Session::has('success'))
            <div class="alert alert-success">{{ Session::get('success') }}</div>
          @endif


              <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                  <a href="{{url('entries')}}">
                    <div class="info-box bg-light-green hover-expand-effect">
                      <div class="icon">
                        <i class="material-icons">loyalty</i>
                      </div>
                      <div class="content">
                        <div class="text">ENTRIES</div>
                      </div>
                    </div>
                  </a>
              </div>

              <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                  <a href="{{url('inbox')}}">
                      <div class="info-box bg-pink hover-expand-effect">
                          <div class="icon">
                            <i class="material-icons">textsms</i>
                          </div>
                          <div class="content">
                            <div class="text">MESSAGES</div>
                          </div>
                      </div>
                  </a>
              </div>

              <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                  <a href="{{url('withdrawals')}}">
                    <div class="info-box bg-orange hover-expand-effect">
                      <div class="icon">
                        <i class="material-icons">assessment</i>
                      </div>
                      <div class="content">
                        <div class="text">WITHDRAWALS</div>
                      </div>
                    </div>
                  </a>
              </div>

              <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                  <a href="{{url('deposits')}}">
                    <div class="info-box bg-blue hover-expand-effect">
                      <div class="icon">
                        <i class="material-icons">credit_card</i>
                      </div>
                      <div class="content">
                        <div class="text">DEPOSITS</div>
                      </div>
                    </div>
                  </a>
              </div>

        </div>
        <!-- #END# Widgets -->


    </div>
  </section>

@endsection


@section('css')
  <!-- Morris Chart Css-->
  <link href="{{url('plugins/morrisjs/morris.css')}}" rel="stylesheet" />
@endsection

@section('scripts')
  <script src="{{url('plugins/jquery-sparkline/jquery.sparkline.js')}}"></script>
  <!-- Morris Plugin Js -->
  <script src="{{url('plugins/raphael/raphael.min.js')}}"></script>
  <script src="{{url('plugins/morrisjs/morris.js')}}"></script>

  <!-- ChartJs -->
  <script src="{{url('plugins/chartjs/Chart.bundle.js')}}"></script>





@endsection
