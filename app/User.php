<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email','user_group', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    /* relationships. */
    public function role()
    {
        return $this->belongsTo('App\UserGroup', 'user_group');
    }

    public function get_user_group()
    {
        return $this->belongsTo('App\UserGroup', 'user_group');
    }

    public function artist_data(){
        return $this->hasOne('App\Artist');
    }

}
