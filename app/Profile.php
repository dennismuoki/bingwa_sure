<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = 'profile';
    protected $primaryKey = 'profile_id';

//    const CREATED_AT = 'created';
//    const UPDATED_AT = 'modified';
}
