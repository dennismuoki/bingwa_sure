<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Outbox extends Model
{
    protected $table = 'outbox';
    protected $primaryKey = 'outbox_id';

//    const CREATED_AT = 'created';
//    const UPDATED_AT = 'modified';
}
