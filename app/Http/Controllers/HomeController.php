<?php

namespace App\Http\Controllers;

use App\Deposit;
use App\Entry;
use App\Profile;
use App\Winner;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if (Auth::user()->user_group == 4){
            return view('cs_dashboard');
        }else{
            $deposits = DB::table('deposit')->select(DB::raw('DATE(created_at) as created_day'), DB::raw(' SUM(amount) as amount'))
                ->groupBy('created_day')
                ->orderBy('created_day', 'desc')
                ->limit(7)
                ->get()
                ->reverse();

            return view('dashboard', ['deposits' => $deposits]);
        }

    }

    public function  get_new_subs(){
        $message = \App\Profile::whereDate('created_at', DB::raw('CURDATE()'))->count();
        echo json_encode(["error" => false, "message" => $message]);
        return;
    }

    public function  get_total_subs(){
        $message = \App\Profile::count();
        echo json_encode(["error" => false, "message" => $message]);
        return;
    }

    public function  get_deposits(){
        $message = \App\Deposit::whereDate('created_at', DB::raw('CURDATE()'))->sum('amount');
        echo json_encode(["error" => false, "message" => $message]);
        return;
    }

    public function  get_profit(){

        $stake = DB::table('entry')->whereDate('created_at',Carbon::now()->toDateString())->sum('stake');
        $wins = DB::table('winner')->whereDate('created_at',Carbon::now()->toDateString())->sum('win_amount');

        echo json_encode(["error" => false, "message" => $stake-$wins]);
        return;
    }

    public function  get_winnings(){

        $winnings = Winner::select(DB::raw('DATE(created_at) as created_day,  SUM(win_amount) as amount'))
            ->groupBy('created_day')
            ->orderBy('created_day', 'desc')
            ->limit(7)
            ->get();

        $list = "";

        foreach ($winnings as $winning){
            $list .= "<li>";
            $list .= Carbon::parse($winning->created_day)->formatLocalized('%A %d %B %Y');
            $list .= "<span class='pull-right'><small>KSH</small><b>";
            $list .= $winning->amount;
            $list .= "</b></span>";
            $list .= "</li>";
        }

        echo json_encode(["error" => false, "message" => $list]);
        return;
    }

    public function  get_entries(){

        $entries = Entry::select(DB::raw('DATE(created_at) as created_day,  SUM(stake) as amount'))
            ->groupBy('created_day')
            ->orderBy('created_day', 'desc')
            ->limit(7)
            ->get();


        $list = "";

        foreach ($entries as $entry){
            $list .= "<li>";
            $list .= Carbon::parse($entry->created_day)->formatLocalized('%A %d %B %Y');
            $list .= "<span class='pull-right'><small>KSH</small><b>";
            $list .= $entry->amount;
            $list .= "</b></span>";
            $list .= "</li>";
        }

        echo json_encode(["error" => false, "message" => $list]);
        return;
    }

    public function get_depositors_summary(){
        $newSubs = DB::table('deposit')->join('profile','deposit.profile_id','profile.profile_id')
            ->select(DB::raw('SUM(deposit.amount) as new_subs_deposits, count(distinct profile.profile_id) as new_subs'))
            ->whereDate('profile.created_at',\Carbon\Carbon::now()->toDateString())
            ->first();

        $existingSubs = DB::table('deposit')->join('profile','deposit.profile_id','profile.profile_id')
            ->select(DB::raw('SUM(deposit.amount) as exist_subs_deposits, count(distinct profile.profile_id) as exist_subs'))
            ->whereDate('profile.created_at','!=',\Carbon\Carbon::now()->toDateString())
            ->whereDate('deposit.created_at',\Carbon\Carbon::now()->toDateString())
            ->first();

        $todaysDeposits = DB::table('deposit')->whereDate('created_at',\Carbon\Carbon::now()->toDateString())->sum('amount');
        $yesterdaysDeposits = DB::table('deposit')->whereDate('created_at',\Carbon\Carbon::now()->subDay(1)->toDateString())->whereRaw('created_at < DATE_SUB(now(), INTERVAL 24 HOUR)')->sum('amount');


        if($yesterdaysDeposits > 0){
            $previousDayPercentage = ($todaysDeposits-$yesterdaysDeposits)/$yesterdaysDeposits;

        }else{
            $previousDayPercentage=0;
        }
        $previousDayBackground = $previousDayPercentage <= 0 ? 'btn-danger' : 'btn-success';
        $keyBoardArrow = $previousDayPercentage <= 0 ? 'keyboard_arrow_down' : 'keyboard_arrow_up';
        $previousDayFormatedPercentage = sprintf("%.2f%%",$previousDayPercentage*100);

        $percentageButton = "<button type='button' class='btn $previousDayBackground waves-effect'>";
        $percentageButton .= "<i class='material-icons' style='color: white'>$keyBoardArrow</i>";
        $percentageButton .= "<span>";
        $percentageButton .= $previousDayFormatedPercentage;
        $percentageButton .= " </span>";
        $percentageButton .= "</button>";




        $totalDepositors = $newSubs->new_subs + $existingSubs->exist_subs;
        $totalDeposits =  $newSubs->new_subs_deposits + $existingSubs->exist_subs_deposits;
        $percentage =  ($totalDeposits - Config::get('app.target'))/Config::get('app.target');
        $background = $percentage >0 ? 'bg-green' : 'bg-red';
        $formatedPercentage = sprintf("%.2f%%",$percentage*100);


        $table = "<table class=\"table table-striped\">";
        $table .= "<tbody>";

        $table .= "<tr>";
        $table .= "<td>New</td>";
        $table .= "<td>$newSubs->new_subs</td>";
        $table .= "<td> Ksh. $newSubs->new_subs_deposits</td>";
        $table .= "</tr>";

        $table .= "<tr>";
        $table .= "<td>Existing</td>";
        $table .= "<td>$existingSubs->exist_subs</td>";
        $table .= "<td>Ksh. $existingSubs->exist_subs_deposits</td>";
        $table .= "</tr>";

        $table .= "<tr>";
        $table .= "<td><b>Total</b></td>";
        $table .= "<td><b>$totalDepositors</b></td>";
        $table .= "<td><b>Ksh. $totalDeposits</b></td>";
        $table .= "</tr>";

        $table .= "<tr>";
        $table .= "<td><b>Target</b></td>";
        $table .= "<td><b>Ksh.". Config::get('app.target')."</b></td>";
        $table .= "<td><b class='$background' style=\"padding: 5px\">$formatedPercentage</b></td>";
        $table .= "</tr>";

        $table .= "</tbody>";
        $table .= "</table>";


        echo json_encode(["error" => false, "table" => $table, "percentageButton" => $percentageButton ]);
        return;
    }

    public function get_stakes_summary(){

        $newSubsStake = DB::table('entry')->join('profile','entry.profile_id','profile.profile_id')
            ->select(DB::raw('SUM(entry.stake) as new_subs_stake, count(distinct profile.profile_id) as new_subs'))
            ->whereDate('profile.created_at',\Carbon\Carbon::now()->toDateString())
            ->first();

        $existingSubsStake = DB::table('entry')->join('profile','entry.profile_id','profile.profile_id')
            ->select(DB::raw('SUM(entry.stake) as exist_subs_stake, count(distinct profile.profile_id) as exist_subs'))
            ->whereDate('profile.created_at','!=',\Carbon\Carbon::now()->toDateString())
            ->whereDate('entry.created_at',\Carbon\Carbon::now()->toDateString())
            ->first();

        $todaysStakes = DB::table('entry')->whereDate('created_at',\Carbon\Carbon::now()->toDateString())->sum('stake');
        $yesterdaysStakes = DB::table('entry')->whereDate('created_at',\Carbon\Carbon::now()->subDay(1)->toDateString())->whereRaw('created_at < DATE_SUB(now(), INTERVAL 24 HOUR)')->sum('stake');


        if($yesterdaysStakes > 0){
            $previousDayPercentage = ($todaysStakes-$yesterdaysStakes)/$yesterdaysStakes;

        }else{
            $previousDayPercentage=0;
        }
        $previousDayBackground = $previousDayPercentage <= 0 ? 'btn-danger' : 'btn-success';
        $keyBoardArrow = $previousDayPercentage <= 0 ? 'keyboard_arrow_down' : 'keyboard_arrow_up';
        $previousDayFormatedPercentage = sprintf("%.2f%%",$previousDayPercentage*100);

        $percentageButton = "<button type='button' class='btn $previousDayBackground waves-effect'>";
        $percentageButton .= "<i class='material-icons' style='color: white'>$keyBoardArrow</i>";
        $percentageButton .= "<span>";
        $percentageButton .= $previousDayFormatedPercentage;
        $percentageButton .= " </span>";
        $percentageButton .= "</button>";



        $totalSubs = $newSubsStake->new_subs + $existingSubsStake->exist_subs;
        $totalSubsStakes =  $newSubsStake->new_subs_stake + $existingSubsStake->exist_subs_stake;
        $percentage =  ($totalSubsStakes - Config::get('app.target'))/Config::get('app.target');
        $background = $percentage >0 ? 'bg-green' : 'bg-red';
        $formatedPercentage = sprintf("%.2f%%",$percentage*100);



        $table = "<table class=\"table table-striped\">";
        $table .= "<tbody>";

        $table .= "<tr>";
        $table .= "<td>New</td>";
        $table .= "<td>$newSubsStake->new_subs</td>";
        $table .= "<td> Ksh. $newSubsStake->new_subs_stake</td>";
        $table .= "</tr>";

        $table .= "<tr>";
        $table .= "<td>Existing</td>";
        $table .= "<td>$existingSubsStake->exist_subs</td>";
        $table .= "<td>Ksh. $existingSubsStake->exist_subs_stake</td>";
        $table .= "</tr>";

        $table .= "<tr>";
        $table .= "<td><b>Total</b></td>";
        $table .= "<td><b>$totalSubs</b></td>";
        $table .= "<td><b>Ksh. $totalSubsStakes</b></td>";
        $table .= "</tr>";

        $table .= "<tr>";
        $table .= "<td><b>Target</b></td>";
        $table .= "<td><b>Ksh.". Config::get('app.target')."</b></td>";
        $table .= "<td><b class='$background' style=\"padding: 5px\">$formatedPercentage</b></td>";
        $table .= "</tr>";

        $table .= "</tbody>";
        $table .= "</table>";


        echo json_encode(["error" => false, "table" => $table, "percentageButton" => $percentageButton ]);
        return;
    }

    public function get_winners_summary(){

        $newSubsWins = \App\Profile::join('entry','profile.profile_id','entry.profile_id')
            ->join('winner','entry.entry_id','winner.entry_id')
            ->select(DB::raw('SUM(winner.win_amount) as new_subs_wins, count(distinct profile.profile_id) as new_subs'))
            ->whereDate('profile.created_at',\Carbon\Carbon::now()->toDateString())
            ->whereDate('winner.created_at',\Carbon\Carbon::now()->toDateString())
            ->first();



        $existingSubsWins = \App\Profile::join('entry','profile.profile_id','entry.profile_id')
            ->join('winner','entry.entry_id','winner.entry_id')
            ->select(DB::raw('SUM(winner.win_amount) as exist_subs_wins, count(distinct profile.profile_id) as exist_subs'))
            ->whereDate('winner.created_at',\Carbon\Carbon::now()->toDateString())
            ->whereDate('profile.created_at','!=',\Carbon\Carbon::now()->toDateString())
            ->first();

        $todaysWins = DB::table('winner')->whereDate('created_at',\Carbon\Carbon::now()->toDateString())->sum('win_amount');
        $yesterdaysWins = DB::table('winner')->whereDate('created_at',\Carbon\Carbon::now()->subDay(1)->toDateString())->whereRaw('created_at < DATE_SUB(now(), INTERVAL 24 HOUR)')->sum('win_amount');


        if($yesterdaysWins > 0){
            $previousDayPercentage = ($todaysWins-$yesterdaysWins)/$yesterdaysWins;

        }else{
            $previousDayPercentage=0;
        }
        $previousDayBackground = $previousDayPercentage <= 0 ? 'btn-danger' : 'btn-success';
        $keyBoardArrow = $previousDayPercentage <= 0 ? 'keyboard_arrow_down' : 'keyboard_arrow_up';
        $previousDayFormatedPercentage = sprintf("%.2f%%",$previousDayPercentage*100);

        $percentageButton = "<button type='button' class='btn $previousDayBackground waves-effect'>";
        $percentageButton .= "<i class='material-icons' style='color: white'>$keyBoardArrow</i>";
        $percentageButton .= "<span>";
        $percentageButton .= $previousDayFormatedPercentage;
        $percentageButton .= " </span>";
        $percentageButton .= "</button>";


        $totalSubs = $newSubsWins->new_subs + $existingSubsWins->exist_subs;
        $totalSubsWins =  $newSubsWins->new_subs_wins + $existingSubsWins->exist_subs_wins;



        $stake = DB::table('entry')->whereDate('created_at',Carbon::now()->toDateString())->sum('stake');
        $wins = DB::table('winner')->whereDate('created_at',Carbon::now()->toDateString())->sum('win_amount');

        $rate = "<b class='bg-blue' style='padding: 5px'>";
        $rate .= sprintf("%.2f%%", $stake > 0 ? ($wins/$stake)*100 : 0);
        $rate .= " </b>";





        $table = "<table class=\"table table-striped\">";
        $table .= "<tbody>";

        $table .= "<tr>";
        $table .= "<td>New</td>";
        $table .= "<td>$newSubsWins->new_subs</td>";
        $table .= "<td> Ksh. $newSubsWins->new_subs_wins</td>";
        $table .= "</tr>";

        $table .= "<tr>";
        $table .= "<td>Existing</td>";
        $table .= "<td>$existingSubsWins->exist_subs</td>";
        $table .= "<td>Ksh. $existingSubsWins->exist_subs_wins</td>";
        $table .= "</tr>";

        $table .= "<tr>";
        $table .= "<td><b>Total</b></td>";
        $table .= "<td><b>$totalSubs</b></td>";
        $table .= "<td><b>Ksh. $totalSubsWins</b></td>";
        $table .= "</tr>";

        $table .= "<tr>";
        $table .= "<td><b>Winning Rate</b></td>";
        $table .= "<td><b> </b></td>";
        $table .= "<td>$rate</td>";
        $table .= "</tr>";

        $table .= "</tbody>";
        $table .= "</table>";


        echo json_encode(["error" => false, "table" => $table, "percentageButton" => $percentageButton ]);
        return;

    }
}
