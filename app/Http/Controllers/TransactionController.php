<?php

namespace App\Http\Controllers;

use App\Deposit;
use App\Withdrawal;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use ViewComponents\Eloquent\EloquentDataProvider;
use ViewComponents\Grids\Component\Column;
use ViewComponents\Grids\Component\ColumnSortingControl;
use ViewComponents\Grids\Component\CsvExport;
use ViewComponents\Grids\Component\PageTotalsRow;
use ViewComponents\Grids\Component\TableCaption;
use ViewComponents\Grids\Grid;
use ViewComponents\ViewComponents\Component\Control\FilterControl;
use ViewComponents\ViewComponents\Component\Control\PaginationControl;
use ViewComponents\ViewComponents\Customization\CssFrameworks\BootstrapStyling;
use ViewComponents\ViewComponents\Data\Operation\FilterOperation;
use ViewComponents\ViewComponents\Input\InputSource;

class TransactionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function deposits()
    {
        $provider = new EloquentDataProvider(Deposit::orderBy('deposit_id','desc'));

        $input = new InputSource($_GET);

        // create grid
        $grid = new Grid(
            $provider,
            // all components are optional, you can specify only columns
            [
                new TableCaption('Deposits'),
                new Column('created_at', 'Date'),
                new Column('name', 'Name'),
                new Column('msisdn', 'MSISDN'),
                new Column('amount','Amount'),
                new Column('status','Status'),
                new Column('reference_id','Reference'),

                new PaginationControl($input->option('page', 1), 20), // 1 - default page, 5 -- page size
                new ColumnSortingControl('created_at', $input->option('sort')),
                $date =  new FilterControl('created_at', FilterOperation::OPERATOR_STR_CONTAINS, $input->option('created_at')),
                $nameFilter = new FilterControl('name', FilterOperation::OPERATOR_STR_CONTAINS, $input->option('name')),
                $msisdn = new FilterControl('msisdn', FilterOperation::OPERATOR_LIKE, $input->option('msisdn')),
                $amount = new FilterControl('amount', FilterOperation::OPERATOR_LIKE, $input->option('amount')),
                $status = new FilterControl('status', FilterOperation::OPERATOR_LIKE, $input->option('status')),
                $reference_id = new FilterControl('reference_id', FilterOperation::OPERATOR_STR_CONTAINS, $input->option('reference_id')),
                $csv = new CsvExport($input->option('csv')), // yep, that's so simple, you have CSV export now
                $csv->setFileName('Deposits.csv'),
                new PageTotalsRow([
                    'amount' => PageTotalsRow::OPERATION_SUM
                ])
            ]
        );


        $date->getView()->setDataItem('placeholder', 'Date');
        $date->getView()->setDataItem('label', '');
        $date->getView()->setDataItem('inputType', 'date');


        $msisdn->getView()->setDataItem('placeholder', 'Phone No');
        $msisdn->getView()->setDataItem('label', '');

        $nameFilter->getView()->setDataItem('placeholder', 'Name');
        $nameFilter->getView()->setDataItem('label', '');

        $amount->getView()->setDataItem('placeholder', 'Amount');
        $amount->getView()->setDataItem('label', '');

        $reference_id->getView()->setDataItem('placeholder', 'Reference');
        $reference_id->getView()->setDataItem('label', '');


        $status->getView()->setDataItem('placeholder', 'Status');
        $status->getView()->setDataItem('label', '');




        $customization = new BootstrapStyling();
        $customization->apply($grid);


        $grid = $grid->render();
        return view('deposits', compact('grid'));

    }

    public function withdrawals()
    {
        $provider = new EloquentDataProvider(Withdrawal::orderBy('withdrawal_id','desc'));

        $input = new InputSource($_GET);

        // create grid
        $grid = new Grid(
            $provider,
            // all components are optional, you can specify only columns
            [
                new TableCaption('Withdrawals'),
                new Column('created', 'Date'),
                new Column('msisdn', 'MSISDN'),
                new Column('amount','Amount'),
                new Column('status','Status'),

                new PaginationControl($input->option('page', 1), 20), // 1 - default page, 5 -- page size
                new ColumnSortingControl('created', $input->option('sort')),
                $date =  new FilterControl('created', FilterOperation::OPERATOR_STR_CONTAINS, $input->option('created')),
                $msisdn = new FilterControl('msisdn', FilterOperation::OPERATOR_LIKE, $input->option('msisdn')),
                $amount = new FilterControl('amount', FilterOperation::OPERATOR_LIKE, $input->option('amount')),
                $status = new FilterControl('status', FilterOperation::OPERATOR_LIKE, $input->option('status')),
                $csv = new CsvExport($input->option('csv')), // yep, that's so simple, you have CSV export now
                $csv->setFileName('Withdrawals.csv'),
                new PageTotalsRow([
                    'amount' => PageTotalsRow::OPERATION_SUM
                ])
            ]
        );


        $date->getView()->setDataItem('placeholder', 'Date');
        $date->getView()->setDataItem('label', '');
        $date->getView()->setDataItem('inputType', 'date');


        $msisdn->getView()->setDataItem('placeholder', 'Phone No');
        $msisdn->getView()->setDataItem('label', '');

        $status->getView()->setDataItem('placeholder', 'Status');
        $status->getView()->setDataItem('label', '');

        $amount->getView()->setDataItem('placeholder', 'Amount');
        $amount->getView()->setDataItem('label', '');


        $customization = new BootstrapStyling();
        $customization->apply($grid);


        $grid = $grid->render();
        return view('withdrawals', compact('grid'));

    }
}
