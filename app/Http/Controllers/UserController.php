<?php

namespace App\Http\Controllers;

use App\Permission;
use App\User;
use App\UserGroup;
use App\UserPermission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;


class UserController extends Controller
{
    protected $userGroup;
    protected $user;
    protected $userPermission;
    protected $random_pass;


    public function __construct()
    {
        $this->middleware('auth');
        $this->userGroup = new UserGroup();
        $this->user = new User();
        $userPermission = new UserPermission();
    }

    public function index()
    {

        $users = User::paginate(10);

        return view('users.index', ['users' => $users]);

    }

    function register_user(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:50',
            'user_group_id' => 'required|max:10',
            'email' => 'required|email|max:255|unique:users,email'
        ]);

        $this->random_pass = $this->randomPassword();
        $this->user->name = $request->name;
        $this->user->user_group = $request->user_group_id;
        $this->user->email = $request->email;
        $this->user->password = bcrypt($this->random_pass);


        DB::transaction(function () {
            if ($data = $this->user->saveOrFail()) {
                $this->user->raw_password = $this->random_pass;
                (new MailerController())->welcome_user($this->user);
                Session::flash("success", "User created Successfully!");

            }
        });

        return redirect('/users');
    }

    function randomPassword() {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array();
        $alphaLength = strlen($alphabet) - 1;
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass);
    }

    public function getIndex(Request $request)
    {
        $this->validate($request, [
            'search' => 'required'
        ]);

        $search = $request->get('search');

        $users = User::where('name', 'like', "%$search%")
            ->orWhere('email', 'like', "%$search%")
            ->orWhere('id', 'like', "%$search%")
            ->paginate(10);

        return view('users.index', compact('users'));
    }

    function profile($user_id)
    {
        $user = User::find($user_id);
        return view('users.profile')->with('user', $user);
    }

    public function update()
    {
        $request = (object)$_POST;
        $this->user = User::find($request->user_id);

        $this->user->name = $request->name;
        $this->user->email = $request->email;
        $this->user->user_group = $request->user_group;


        DB::transaction(function () {
            if ($data = $this->user->update()) {
                Session::flash("success", "Updated Successfully!");
            }
        });

        return redirect('/users/profile/' . $request->user_id);
    }

    public function groups()
    {
        return view('users.groups')->with(array('groups' => UserGroup::all(), 'permisions' => Permission::all()));
    }

    function register_group()
    {
        DB::transaction(function () {

            $request = (object)$_POST;
            $this->userGroup->name = $request->name;
            $this->userGroup->description = $request->desc;


            if ($data = $this->userGroup->saveOrFail()) {

                foreach ($_POST['permission'] as $perm) {
                    $userPermission = new UserPermission();
                    $userPermission->user_group = $this->userGroup->id;
                    $userPermission->permission = $perm;
                    $data2 = $userPermission->saveOrFail();
                }
            }
        });
        return redirect('/users/groups');
    }

    function group_profile($group_id)
    {
        $userGroup = UserGroup::find($group_id);
        $user_permissions = UserPermission::where('user_group', $group_id)->cursor();
        return view('users.usergroup_profile')->with(array('usergroup' => $userGroup, 'user_permissions' => $user_permissions));
    }

    function delete_group_permission($group_id)
    {
        $this->userPermission = UserPermission::find($group_id);
        DB::transaction(function () {
            if ($data = $this->userPermission->delete()) {
                Session::flash("success", "Permission deleted successfully.");
            }
        });
        return redirect()->back();
    }

    function delete_user($user_id)
    {
        $this->user = User::find($user_id);
        DB::transaction(function () {
            if ($data = $this->user->delete()) {
                Session::flash("success", "Deleted Successfully!");

            }
        });
        return redirect('/users');
    }
    public function update_group()
    {
        $request = (object)$_POST;
        $this->userGroup = UserGroup::find($request->group_id);
        $this->userGroup->name = $request->name;
        $this->userGroup->description = $request->description;
        DB::transaction(function () {
            if ($data = $this->userGroup->update()) {
                Session::flash("success", "Updated Successfully!");

            }
        });

        return redirect()->back();
    }

    public function add_group_permission(Request $request)
    {
        $this->validate($request, [
            'permission' => 'bail|required',
            'group_id' => 'bail|required',
        ]);

        foreach ($_POST['permission'] as $perm) {
            $this->userPermission = new userPermission();
            $this->userPermission->user_group = $request->group_id;
            $this->userPermission->permission = $perm;
            $data = $this->userPermission->saveOrFail();
        }
        return redirect()->back();
    }
}
