<?php

namespace App\Http\Controllers;

use App\Inbox;
use App\Outbox;

use App\Deposit;
use App\Withdrawal;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use ViewComponents\Eloquent\EloquentDataProvider;
use ViewComponents\Grids\Component\Column;
use ViewComponents\Grids\Component\ColumnSortingControl;
use ViewComponents\Grids\Component\CsvExport;
use ViewComponents\Grids\Component\PageTotalsRow;
use ViewComponents\Grids\Component\TableCaption;
use ViewComponents\Grids\Grid;
use ViewComponents\ViewComponents\Component\Control\FilterControl;
use ViewComponents\ViewComponents\Component\Control\PaginationControl;
use ViewComponents\ViewComponents\Component\TemplateView;
use ViewComponents\ViewComponents\Customization\CssFrameworks\BootstrapStyling;
use ViewComponents\ViewComponents\Data\Operation\FilterOperation;
use ViewComponents\ViewComponents\Input\InputSource;



class MessagesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function inbox()
    {
        $provider = new EloquentDataProvider(Inbox::orderBy('inbox_id','desc'));

        $input = new InputSource($_GET);

        // create grid
        $grid = new Grid(
            $provider,
            // all components are optional, you can specify only columns
            [
                new TableCaption('Inbox'),
                new Column('created_at', 'Date'),
                new Column('msisdn', 'MSISDN'),
                new Column('text','Text'),
                new Column('response','Response'),

                new PaginationControl($input->option('page', 1), 20), // 1 - default page, 5 -- page size
                new ColumnSortingControl('created_at', $input->option('sort')),
                $date =  new FilterControl('created_at', FilterOperation::OPERATOR_STR_CONTAINS, $input->option('created_at')),
                $msisdn = new FilterControl('msisdn', FilterOperation::OPERATOR_LIKE, $input->option('msisdn')),
                $text = new FilterControl('text', FilterOperation::OPERATOR_LIKE, $input->option('text')),
                $response = new FilterControl('response', FilterOperation::OPERATOR_LIKE, $input->option('response')),
                $csv = new CsvExport($input->option('csv')), // yep, that's so simple, you have CSV export now
                $csv->setFileName('Inbox.csv')
            ]
        );


        $date->getView()->setDataItem('placeholder', 'Date');
        $date->getView()->setDataItem('label', '');
        $date->getView()->setDataItem('inputType', 'date');


        $msisdn->getView()->setDataItem('placeholder', 'Phone No');
        $msisdn->getView()->setDataItem('label', '');


        $text->getView()->setDataItem('placeholder', 'Text');
        $text->getView()->setDataItem('label', '');


        $response->getView()->setDataItem('placeholder', 'Response');
        $response->getView()->setDataItem('label', '');


        $customization = new BootstrapStyling();
        $customization->apply($grid);


        $grid = $grid->render();
        return view('inbox', compact('grid'));

    }

    public function outbox()
    {

        $provider = new EloquentDataProvider(Outbox::orderBy('outbox_id','desc'));

        $input = new InputSource($_GET);

        // create grid
        $grid = new Grid(
            $provider,
            // all components are optional, you can specify only columns
            [
                new TableCaption('Outbox'),
                new Column('created_at', 'Date'),
                new Column('msisdn', 'MSISDN'),
                new Column('text','Text'),
                new Column('status','Status'),

                new PaginationControl($input->option('page', 1), 20), // 1 - default page, 5 -- page size
                new ColumnSortingControl('created_at', $input->option('sort')),
                $date =  new FilterControl('created_at', FilterOperation::OPERATOR_STR_CONTAINS, $input->option('created_at')),
                $msisdn = new FilterControl('msisdn', FilterOperation::OPERATOR_LIKE, $input->option('msisdn')),
                $text = new FilterControl('text', FilterOperation::OPERATOR_LIKE, $input->option('text')),
                $status = new FilterControl('status', FilterOperation::OPERATOR_LIKE, $input->option('status')),
                $csv = new CsvExport($input->option('csv')), // yep, that's so simple, you have CSV export now
                $csv->setFileName('Outbox.csv')
            ]
        );


        $date->getView()->setDataItem('placeholder', 'Date');
        $date->getView()->setDataItem('label', '');
        $date->getView()->setDataItem('inputType', 'date');


        $msisdn->getView()->setDataItem('placeholder', 'Phone No');
        $msisdn->getView()->setDataItem('label', '');


        $text->getView()->setDataItem('placeholder', 'Text');
        $text->getView()->setDataItem('label', '');


        $status->getView()->setDataItem('placeholder', 'Status');
        $status->getView()->setDataItem('label', '');

        $customization = new BootstrapStyling();
        $customization->apply($grid);


        $grid = $grid->render();
        return view('outbox', compact('grid'));


    }
}
