<?php

namespace App\Http\Controllers;

use App\Draw;
use App\Entry;
use App\Winner;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use ViewComponents\Eloquent\EloquentDataProvider;
use ViewComponents\Grids\Component\Column;
use ViewComponents\Grids\Component\ColumnSortingControl;
use ViewComponents\Grids\Component\CsvExport;
use ViewComponents\Grids\Component\PageTotalsRow;
use ViewComponents\Grids\Component\TableCaption;
use ViewComponents\Grids\Grid;
use ViewComponents\ViewComponents\Component\Control\FilterControl;
use ViewComponents\ViewComponents\Component\Control\PaginationControl;
use ViewComponents\ViewComponents\Customization\CssFrameworks\BootstrapStyling;
use ViewComponents\ViewComponents\Data\Operation\FilterOperation;
use ViewComponents\ViewComponents\Input\InputSource;

class DrawController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function draws()
    {

        $draws = Draw::paginate(20);
        return view('draws', ['draws' => $draws]);
    }

    public function entries()
    {

        $provider = new EloquentDataProvider(Entry::leftJoin('profile', 'entry.profile_id', '=', 'profile.profile_id')
                                                    ->orderBy('entry.entry_id','desc')
                                                    ->select('entry.created_at','profile.msisdn', 'entry.stake',
                                                        'entry.choice','entry.play_status'));

        $input = new InputSource($_GET);

        // create grid
        $grid = new Grid(
            $provider,
            // all components are optional, you can specify only columns
            [
                new TableCaption('Entries'),
                new Column('created_at', 'Date'),
                new Column('msisdn', 'MSISDN'),
                new Column('stake','Stake'),
                new Column('choice','Choice'),
                new Column('status','Status'),

                new PaginationControl($input->option('page', 1), 20), // 1 - default page, 5 -- page size
                new ColumnSortingControl('created_at', $input->option('sort')),
                $date =  new FilterControl('created_at', FilterOperation::OPERATOR_STR_CONTAINS, $input->option('entry.created_at')),
                $msisdn = new FilterControl('msisdn', FilterOperation::OPERATOR_LIKE, $input->option('profile.msisdn')),
                $stake = new FilterControl('stake', FilterOperation::OPERATOR_LIKE, $input->option('entry.stake')),
                $choice = new FilterControl('choice', FilterOperation::OPERATOR_LIKE, $input->option('entry.choice')),
                $status = new FilterControl('status', FilterOperation::OPERATOR_LIKE, $input->option('entry.play_status')),
                $csv = new CsvExport($input->option('csv')), // yep, that's so simple, you have CSV export now
                $csv->setFileName('Entries.csv'),
                new PageTotalsRow([
                    'stake' => PageTotalsRow::OPERATION_SUM
                ])
            ]
        );


        $date->getView()->setDataItem('placeholder', 'Date');
        $date->getView()->setDataItem('label', '');
        $date->getView()->setDataItem('inputType', 'date');


        $msisdn->getView()->setDataItem('placeholder', 'Phone No');
        $msisdn->getView()->setDataItem('label', '');


        $stake->getView()->setDataItem('placeholder', 'Stake');
        $stake->getView()->setDataItem('label', '');


        $choice->getView()->setDataItem('placeholder', 'Choice');
        $choice->getView()->setDataItem('label', '');


        $status->getView()->setDataItem('placeholder', 'Status');
        $status->getView()->setDataItem('label', '');


        $customization = new BootstrapStyling();
        $customization->apply($grid);


        $grid = $grid->render();
        return view('entries', compact('grid'));

    }

    public function winners()
    {

        $provider = new EloquentDataProvider(Winner::join('entry', 'winner.entry_id', '=', 'entry.entry_id')
                                                    ->join('profile', 'entry.profile_id', '=', 'profile.profile_id')
                                                    ->orderBy('winner.winner_id','desc')
                                                    ->where('winner.win_amount', '>', 0)
                                                    ->select('winner.created_at','profile.msisdn','winner.winner_type', 'winner.win_amount',
                                                        'winner.stake','winner.odd','winner.status'));

        $input = new InputSource($_GET);

        // create grid
        $grid = new Grid(
            $provider,
            // all components are optional, you can specify only columns
            [
                new TableCaption('Winners'),
                new Column('created_at', 'Date'),
                new Column('msisdn', 'MSISDN'),
                new Column('winner_type', 'Win Type'),
                new Column('win_amount','Win Amount'),
                new Column('stake','Stake'),
                new Column('odd','Odd'),
                new Column('status','Status'),

                new PaginationControl($input->option('page', 1), 20), // 1 - default page, 5 -- page size
                new ColumnSortingControl('created_at', $input->option('sort')),
                $date =  new FilterControl('created_at', FilterOperation::OPERATOR_STR_CONTAINS, $input->option('winner.created_at')),
                $msisdn = new FilterControl('msisdn', FilterOperation::OPERATOR_LIKE, $input->option('profile.msisdn')),
                $winner_type = new FilterControl('winner_type', FilterOperation::OPERATOR_LIKE, $input->option('winner.winner_type')),
                $win_amount = new FilterControl('win_amount', FilterOperation::OPERATOR_LIKE, $input->option('winner.win_amount')),
                $stake = new FilterControl('stake', FilterOperation::OPERATOR_LIKE, $input->option('winner.stake')),
                $odd = new FilterControl('odd', FilterOperation::OPERATOR_STR_CONTAINS, $input->option('winner.odd')),
                $status = new FilterControl('status', FilterOperation::OPERATOR_STR_CONTAINS, $input->option('winner.status')),
                $csv = new CsvExport($input->option('csv')), // yep, that's so simple, you have CSV export now
                $csv->setFileName('Winners.csv'),
                new PageTotalsRow([
                    'win_amount' => PageTotalsRow::OPERATION_SUM
                ])
            ]
        );


        $date->getView()->setDataItem('placeholder', 'Date');
        $date->getView()->setDataItem('label', '');
        $date->getView()->setDataItem('inputType', 'date');


        $msisdn->getView()->setDataItem('placeholder', 'Phone No');
        $msisdn->getView()->setDataItem('label', '');

        $winner_type->getView()->setDataItem('placeholder', 'Win Type');
        $winner_type->getView()->setDataItem('label', '');

        $win_amount->getView()->setDataItem('placeholder', 'Win Amount');
        $win_amount->getView()->setDataItem('label', '');

        $stake->getView()->setDataItem('placeholder', 'Stake');
        $stake->getView()->setDataItem('label', '');

        $odd->getView()->setDataItem('placeholder', 'Odd');
        $odd->getView()->setDataItem('label', '');

        $status->getView()->setDataItem('placeholder', 'Status');
        $status->getView()->setDataItem('label', '');

        $customization = new BootstrapStyling();
        $customization->apply($grid);


        $grid = $grid->render();
        return view('winners', compact('grid'));
    }
}
