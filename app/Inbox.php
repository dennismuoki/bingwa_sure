<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inbox extends Model
{
    protected $table = 'inbox';
    protected $primaryKey = 'inbox_id';

//    const CREATED_AT = 'created';
//    const UPDATED_AT = 'modified';
}
