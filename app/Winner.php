<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Winner extends Model
{
    protected $table = 'winner';
    protected $primaryKey = 'winner_id';

//    const CREATED_AT = 'created';
//    const UPDATED_AT = 'modified';
}
